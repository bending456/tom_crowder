"Add spherical inclusions on a regular lattice inside a cylinder"

import numpy as np
import numpy.linalg as la

import simproc.requesthandler.logging as logging
logger=logging.getLogger(__name__)

NUDGE=1e-9

def check_collisions(trial_abc,trial_xyz,inc_eff,cyl_eff,eff_inclusions):
  """Confirm that the trial point is not too close to the cylinder wall or any static inclusions.
  This includes checking that the point is not beyond the cylinder wall.

  Arguments:

    - trial_abc = point to test in abc coordinate system, as tuple
    - trial_xyz = point to test in xyz coordinate system, as numpy array
    - inc_eff = inclusion radius plus half buffer, as float
    - cyl_eff = cylinder radius minus half buffer, as float
    - eff_inclusions = 2D numpy array, each row an inclusion [x,y,z,r]
  
  Returns:

    - valid = boolean, True if point is OK, False if not"""
  #First, confirm that the point is close enough to the cylinder center axis
  polar_rho = np.sqrt(trial_abc[0]**2 + trial_abc[1]**2)
  valid = (polar_rho + inc_eff) < cyl_eff
  #Next, check against all the static inclusions, if any
  if valid and eff_inclusions.shape[0]>0:
    distvecs = eff_inclusions[:,:3] - trial_xyz
    dists = la.norm(distvecs,axis=1)
    limits = eff_inclusions[:,3] + inc_eff
    bool_arr = dists > limits
    valid = all(bool_arr)
    # logger.debug("Completed collision check.",distvecs=distvecs,dists=dists,limits=limits,bool_arr=bool_arr,valid=valid)
  return valid

def generate_lattice_inclusions_in_cylinder(idstart,incrad,cylrad,buffer,pt1,pt2,inclusions,idstep=100):
  """Create points the points

  Arguments:

    - idstart = ID to use fo the first inclusion, as integer
    - incrad = inclusion radius, as float
    - cylrad = cylinder radius, as float
    - buffer = minimum surface separation distance, as float
    - pt1 = coordinates of cylinder axis start point
    - pt2 = coordinates of cylinder axis end point
    - inclusions = non-lattice inclusions to avoid collision with, as list of inclusions,
      each inclusion a tuple (ID, center x, center y, center z, radius)
    - idstep = number to increment the inclusion id by, optional, defaults to 100
  
  Returns:

    - lattice_inclusions = list of inclusions, each inclusion a tuple as for ``inclusions``."""
  #Get the cylinder axis vector, h hat, and its magnitude, H
  endpoints = np.array([pt1,pt2])
  hvec = endpoints[1] - endpoints[0]
  H = la.norm(hvec)
  hhat = hvec/H
  #Get effective dimensions after accounting for separation buffer distances
  hafbuff = buffer/2.0
  cyl_eff = cylrad - hafbuff
  inc_eff = incrad + hafbuff
  H_eff = H - buffer
  #Apply buffer separation to static inclusions by increasing their radius
  eff_inclusions=[]
  for statinc in inclusions:
    newval=list(statinc[1:4])
    newval.append(statinc[4] + hafbuff)
    eff_inclusions.append(newval)
  eff_inclusions=np.array(eff_inclusions)
  #Generate unit vectors a and b that are mutually orthogonal and orthogonal to h hat
  v1 = np.array([1.0,0.0,0.0]) if hhat[0]<0.8 else np.array[0.0,0.0,1.0]
  ahat = v1 - np.dot(v1,hhat)*hhat
  ahat /= la.norm(ahat)
  bhat = np.cross(hhat,ahat)
  #Get the number of lattice points in each dimension
  Nh = int(np.floor(H_eff/2/inc_eff))
  Na = Nb = int(np.floor((cyl_eff+inc_eff)/2/inc_eff - NUDGE))
  #Scale the unit vectors into appropriate step sizes
  ascale = 2 * cyl_eff / (2*Na-1)
  bscale = 2 * cyl_eff / (2*Nb-1)
  cscale = H_eff / Nh
  #Loop over lattice points
  incid=idstart
  lattice_inclusions=[]
  for i in range(0,2*Na-1):
    Ai = (i - Na + 1) * ascale
    for j in range(0,2*Nb-1):
      Bj = (j - Nb + 1) * bscale
      for k in range(0,Nh):
        Ck = (k + 0.5) * cscale + buffer/2
        trial_abc = (Ai,Bj,Ck)
        trial_pt = endpoints[0] + Ai*ahat + Bj*bhat + Ck*hhat
        if check_collisions(trial_abc,trial_pt,inc_eff,cyl_eff,eff_inclusions):
          this_inc=(incid,)+tuple(trial_pt)+(incrad,)
          lattice_inclusions.append(this_inc)
          incid+=idstep
  #Done
  return lattice_inclusions